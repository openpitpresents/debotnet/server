# De Botnet Worker-Server protocol v1 specification

The protocol works via websockets, where the Worker component connects
to a C&C / Server component.

(Note: Resumption does not exist because this network must be very real time.
if a worker crashes, its work units must go somewhere else as soon as possible)

## Notes

 - `Missing[T]` means the field itself can be missing from the dict.
 - `Optional[T]` means the field's value is nullable.

## Query arguments to Server Websocket URL

| field | description | required |
| --: | :-- | :-- |
| v | protocol version (currently 1) | no |

## Message format

All messages/packets in the protocol are JSON encoded, with the following format

| field | type | description |
| --: | :-- | :-- |
| op | int | op code of the packet |
| d | any | data for the op code |

## Opcodes

| op code | name | sent by |
| --: | :-- | :-- |
| 0 | Hello | Server |
| 1 | Identify | Worker |
| 2 | Ready | Server |
| 3 | Heartbeat | Server / Worker |
| 4 | Heartbeat ACK | Server / Worker |
| 5 | Work | Server |
| 6 | Work Response | Worker |
| 7 | Notify | Worker |

### Protocol error codes

| code | meaning | reconnect? |
| --: | :-- | :-- |
| 4000 | general error | yes |
| 4001 | authentication failure | no |
| 4002 | forced reconnect | yes |
| 4003 | invalid message | maybe |
| 4004 | took too long to heartbeat | yes |
| 4005 | invalid query arguments | no |
| 4006 | too many ips being claimed on identify | no |

### Basic protocol flow

 - Worker connects to Server
 - Server sends a Hello packet, and the heartbeat period
    that it will go with, as well as its error margins.
 - Worker has the amount of seconds in the heartbeat to send an Identify packet.
 - Worker sends an Identify packet, containing authentication credentials and
    human-friendly hostname
 - Server replies with Ready, containing Worker ID
 - Server may send Heartbeat every `heartbeat_period` milliseconds
    - Worker must reply with Heartbeat ACK within a `heartbeat_max_latency`
        time period or Server will auto-close the connection

### A note on reconnections

Workers are recommended to implement exponential backoff with jitter
into their connection code.

Variables starting with `retry_` are configurable and left to Workers to decide
upon. Recommendations are:

 - `retry_max_amount`: 30
 - `retry_base_constant`: 0.75
 - `retry_minimum_cap`: 2.0

```
for retry in range(retry_max_amount):
    try:
        connect_to_server()
    except SomeConnectionError:
        sleep_secs = random.uniform(
            0,
            min(
                retry_minimum_cap,
                retry_base_constant * 2 ** retry,
            ),
        )
        
        sleep(sleep_secs)
```

## Op 0 Hello

| field | type | description |
| --: | :-- | :-- |
| `hostname` | string | human-friendly identifier for the Server |
| `heartbeat_period` | int | amount of milliseconds Server will heartbeat with |
| `heartbeat_max_latency` | int | amount of milliseconds Server will wait for a heartbeat response |

## Op 1 Identify

| field | type | description |
| --: | :-- | :-- |
| `token` | str | authentication token |
| `hostname` | str | human-friendly hostname |
| `ip_count` | int | amount of IPs available to be used on the connection |

## Op 2 Ready

| field | type | description |
| --: | :-- | :-- |
| `worker_id` | str | newly assigned worker id |
| `ip_addresses` | str | newly assigned ip addresses for each ip in Identfy's `ip_count` |

## Op 3 Heartbeat

*no data*

## Op 4 Heartbeat ACK

*no data*

## Op 5 Work

Sent by the Server when a new Work unit is to be processed by the Worker

| field | type | description |
| --: | :-- | :-- |
| `id` | str | work id |
| `topic` | str | work topic |
| `data` | any | data wanted to process the work |

## Op 6 Work Response

| field | type | description |
| --: | :-- | :-- |
| `id` | str | work id |
| `recoverable` | Missing[bool] | if the work unit failed and can be redistributed on the network |
| `failure_reason` | Missing[str] | if failure happened, reason can be given |
| `status_code` | int | the response's HTTP status code |
| `response` | any | the response's (json-encodable) body |

- Keep in mind `recoverable` MUST BE missing when the response is a success
    (from the point of view that there's a valid value to give to the client).
    - Set it to `true` if the IP was banned

## `username` Work Topic

Data of List[str] of the list of usernames to be resolved.

Response of type List[UsernameResult] 

### UsernameResult

| field | type | description |
| --: | :-- | :-- |
| `id` | str | player id |
| `name` | str | player username |

## `debug_ipinfo` Work Topic

Empty data (arguments are simply unused, reference uses `[]`).

Returns https://ipinfo.io/json output.

Is a debug endpoint and is therefore only usable on reference implementation when server has `debug: true`. Optional to implement.

## `has_joined` Work Topic

### HasJoinedData

| field | type | description |
| --: | :-- | :-- |
| `username` | str | player username |
| `serverId` | str | server id |
| `ip` | Missing[str] | ip address of the server |

### HasJoinedResponse

The response data follows the structure described in the
[Minecraft Protocol Encryption](https://wiki.vg/Protocol_Encryption#Server) docs

## Op 7 Notify

Sent by the worker to signal an important notification about the
worker's state.

| field | type | description |
| --: | :-- | :-- |
| `t` | int | notification type |
| `data` | Any | notification data |

### Notification type enum

| value of `t` | title |
| --: | :-- |
| 0 | Banned IP |

### Banned IP Notification

Data is a dict with:
| field | type | description |
| --: | :-- | :-- |
| `ip` | str | IP address that was banned |
| `topic` | str | Work topic for the banned IP. |

 - `ip` MUST BE one of the IP addresses given in the Ready packet.
    - It MUST NOT refer to a network (IPv4 or IPv6) address.
 - `topic` signals in which work topic the IP should not be used for anymore.
    Topics may not share IP bans / Ratelimit information.
 - The IP will be automatically unbanned after 5 minutes.
