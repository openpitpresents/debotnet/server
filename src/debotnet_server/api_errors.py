# debotnet: Distributed proxy
# Copyright 2020, lavatech and debotnet contributors
# SPDX-License-Identifier: AGPL-3.0-only


class APIError(Exception):
    status_code = 500

    @property
    def message(self):
        try:
            return self.args[0]
        except IndexError:
            return repr(self)

    @property
    def payload(self):
        return {}


class BadRequest(APIError):
    status_code = 400


class NoWorkers(APIError):
    status_code = 500


class TooManyRetries(APIError):
    status_code = 500


class TimeoutError(APIError):
    """API level timeout error"""

    pass
