# debotnet: Distributed proxy
# Copyright 2020, lavatech and debotnet contributors
# SPDX-License-Identifier: AGPL-3.0-only

import uuid
import random
import logging
from collections import defaultdict
from typing import Any, Tuple, Dict, List, TypedDict
from dataclasses import dataclass

from expiringdict import ExpiringDict
from quart import current_app as app

from .errors import NoWorkers
from .dispatcher import WorkTopic


log = logging.getLogger(__name__)
IpAddress = uuid.UUID


@dataclass
class Worker:
    conn: Any
    hostname: str
    ip_addresses: List[uuid.UUID]


@dataclass
class StatusWorker:
    hostname: str
    ip_count: int


Status = TypedDict("Status", {"ip_count": int, "workers": Dict[str, StatusWorker]})


class WorkerRegistry:
    def __init__(self):
        self.registry: Dict[uuid.UUID, Worker] = {}

        max_ips: int = app.cfg["max_ips"]

        self.ratelimited_ips: Dict[WorkTopic, [IpAddress, bool]] = {
            # 1req/2.5s
            WorkTopic.Username: ExpiringDict(max_len=max_ips, max_age_seconds=2.5),
            # 1req/180ms
            WorkTopic.HasJoined: ExpiringDict(max_len=max_ips, max_age_seconds=0.18),
            # 1req/1s. Technically it's one per 53s but meh, it's a debug endpoint.
            WorkTopic.DebugIpInfo: ExpiringDict(max_len=max_ips, max_age_seconds=1),
        }

        self.banned_ips: Dict[WorkTopic, Dict[IpAddress, bool]] = {
            WorkTopic.Username: ExpiringDict(max_len=max_ips, max_age_seconds=900),
            WorkTopic.HasJoined: ExpiringDict(max_len=max_ips, max_age_seconds=900),
            WorkTopic.DebugIpInfo: ExpiringDict(max_len=max_ips, max_age_seconds=900),
        }

    def get(self, worker_id: uuid.UUID) -> Worker:
        return self.registry[worker_id]

    def get_status(self) -> Status:
        status: Status = {"ip_count": 0, "workers": {}}
        for worker_uuid, worker in self.registry.items():
            ip_count = len(worker.ip_addresses)
            status["ip_count"] += ip_count

            status["workers"][str(worker_uuid)] = {
                "hostname": worker.hostname,
                "ip_count": ip_count,
            }
        return status

    def mark_banned_ip(self, topic: WorkTopic, ip_address: IpAddress) -> None:
        self.banned_ips[topic][ip_address] = True

    def mark_ratelimited_ip(self, topic: WorkTopic, ip_address: IpAddress) -> None:
        self.ratelimited_ips[topic][ip_address] = True

    def put(self, connection, hostname: str, ip_count: int) -> uuid.UUID:
        worker_id = uuid.uuid4()
        log.info(
            "Creating worker %s (hostname=%r, ip_count=%d)",
            worker_id,
            hostname,
            ip_count,
        )
        self.registry[worker_id] = Worker(
            connection, hostname, [uuid.uuid4() for _ in range(ip_count)],
        )
        return worker_id

    def remove(self, worker_id: uuid.UUID) -> None:
        try:
            log.info("Removing worker %s", worker_id)
            self.registry.pop(worker_id)
        except KeyError:
            log.warning("Unknown worker ID to remove: %s", worker_id)

    def calculate_free_ips(self, topic: str, worker_id: uuid.UUID) -> int:
        """Calculate the amount of available IPs on a given
        topic+worker combo."""
        worker = self.registry[worker_id]
        available_ip_count = 0

        # check its ip addresses and if any are banned or ratelimited
        for ip_address in worker.ip_addresses:
            # try except is faster, which is wild, i'll use it to its max
            # on this, since this is a hot path
            try:
                self.banned_ips[topic][ip_address]
                log.debug("IP %s is banned, skipping", ip_address.hex)
                continue
            except KeyError:
                pass

            try:
                self.ratelimited_ips[topic][ip_address]
                log.debug("IP %s is ratelimiting, skipping", ip_address.hex)
                continue
            except KeyError:
                # by this point, we know there is at least one ip address
                # in the worker that's neither banned or ratelimited
                available_ip_count += 1

        return available_ip_count

    def get_wanted_worker_id(self, *, topic: WorkTopic, retries: int = 0) -> uuid.UUID:
        """Get some random wanted worker from the pool of workers.

        This uses the Power-of-Two algorithm, which works as follows:
         - select two random workers
         - choose the one that has the least amount of work assigned

        The "amount of work" measure we are using is the available IPs
        on a given worker. The more IPs available on a worker, the less work
        it is actually doing.

        This algorithm will keep smooth load between all IPs being used in
        the network, the side effect is that multi-IP workers will be selected
        much more likely, while single-ip workers will only receive anything
        when all multi-ip workers have been exhausted. This is a good thing.
        Keep this in mind in your deployments.

        More details about the algorithm can be found on HAProxy:
        https://www.haproxy.com/blog/power-of-two-load-balancing/
        """
        # No workers were available after we retried 20 times.
        if retries > 20:
            raise NoWorkers("Retried too much. No workers found.")

        if not self.registry:
            raise NoWorkers("No connected workers")

        workers = list(self.registry.keys())

        # even when there is a single worker, and those ids would be
        # the same, the algorithm still works
        worker1 = random.choice(workers)
        worker2 = random.choice(workers)

        worker1_ips = self.calculate_free_ips(topic, worker1)
        worker2_ips = self.calculate_free_ips(topic, worker2)

        if worker1_ips == 0 and worker2_ips == 0:
            return self.get_wanted_worker_id(topic=topic, retries=retries + 1)

        # even if they are the same worker, this will still go off
        # one of the branches.
        if worker1_ips >= worker2_ips:
            return worker1
        else:
            return worker2

    def get_wanted_worker(self, topic: WorkTopic) -> Tuple[uuid.UUID, Worker]:
        worker_id = self.get_wanted_worker_id(topic=topic)
        return worker_id, self.registry[worker_id]
