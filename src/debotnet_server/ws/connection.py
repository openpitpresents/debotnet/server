# debotnet: Distributed proxy
# Copyright 2020, lavatech and debotnet contributors
# SPDX-License-Identifier: AGPL-3.0-only

import json
import logging
import uuid
import asyncio
from random import randint
from typing import Any, TypedDict, Optional
from enum import IntEnum

import violet
from quart import current_app as app
from websockets import ConnectionClosedError

from .opcodes import Op
from .errors import UnknownOpCode, WebsocketError, FailedAuth, InvalidData, TooManyIPs
from ..errors import NoWorkers
from ..dispatcher import WorkTopic

log = logging.getLogger(__name__)


Packet = TypedDict("Packet", {"op": int, "d": Any})


class NotificationType(IntEnum):
    BannedIP = 0


class HTTPWorkResponse:
    __slots__ = ("status_code", "data")

    def __init__(self, status_code: int, data: Any):
        self.status_code: int = status_code
        self.data: Any = data

    def __repr__(self):
        return f"<HTTPWorkResponse status_code={self.status_code}>"


class WebsocketConnection:
    def __init__(self, ws):
        self.ws = ws
        self.HANDLERS = {
            Op.Identify: self.handle_identify,
            Op.WorkResponse: self.handle_work_response,
            Op.HeartbeatAck: self.handle_heartbeat_ack,
            Op.Notify: self.handle_notify,
        }

        self.NOTIFICATION_HANDLERS = {NotificationType.BannedIP: self.notif_banned_ip}
        self.our_work_units = {}
        self.connection_id = uuid.uuid4()

        self._hb_period: int = randint(2500, 3500)
        self._hb_latency: int = 500

        self.worker_id: Optional[uuid.UUID] = None

    async def send(self, data: Any) -> None:
        log.debug("sending %r", data)
        await self.ws.send(json.dumps(data))

    async def recv(self) -> Any:
        data = await self.ws.recv()
        try:
            obj = json.loads(data)
        except json.JSONDecodeError:
            raise InvalidData("Invalid JSON")

        log.debug("Receiving %r", obj)
        return obj

    async def send_op(self, op: Op, data: Any) -> None:
        await self.send({"op": op.value, "d": data})

    async def send_hello(self):
        await self.send_op(Op.Hello, {"hostname": app.cfg["name"]})

    @property
    def _heartbeat_task_id(self) -> str:
        return f"heartbeat_spawner:{self.connection_id.hex}"

    @property
    def _heartbeat_hope_task_id(self) -> str:
        return f"heartbeat_hope:{self.connection_id.hex}"

    def push_work(self, work):
        self.our_work_units[work.id] = work
        app.sched.spawn(
            self.send_op,
            [Op.Work, {"id": work.id.hex, "topic": work.typ.value, "data": work.data},],
            name=f"send_work:{work.id.hex}:{self.connection_id.hex}",
        )

    async def _send_hb_and_hope(self):
        await self.send_op(Op.Heartbeat, None)
        await asyncio.sleep(self._hb_latency / 1000)
        # if we already waited latency and this task
        # wasn't cancelled (with a heartbeat ack),
        await self.ws.close(4004, reason="took too long to heartbeat")

    async def _call_heartbeat(self):
        """Wait for a heartbeat, send it, and wait, if no ACK is given, oops.."""
        try:
            app.sched.spawn(
                self._send_hb_and_hope, [], name=self._heartbeat_hope_task_id
            )
        except violet.errors.TaskExistsError:
            log.warning("Sender task was not cancelled. Connection down?")
            return

    async def handle_identify(self, data):
        try:
            assert data is not None
            token = data["token"]
            hostname = data["hostname"]
            ip_count = data["ip_count"]
        except (AssertionError, KeyError):
            raise InvalidData("Invalid identify data")

        if ip_count > app.cfg["max_ips_per_worker"]:
            raise TooManyIPs("Too many IPs being claimed in a single worker")

        if token != app.cfg["token"]:
            raise FailedAuth("Invalid token.")

        worker_id = app.registry.put(self, hostname, ip_count)
        log.info("Registered worker %s", worker_id)

        self.worker_id = worker_id

        worker = app.registry.get(worker_id)
        await self.send_op(
            Op.Ready,
            {
                "worker_id": worker_id.hex,
                "ip_addresses": [ip_id.hex for ip_id in worker.ip_addresses],
            },
        )

    def _try_redispatch(self, work):
        """Try to send the work to another worker in the network, if this
        fails due to no workers being found, the future will be set to
        NoWorkers."""

        log.warning("Redispatching work %s", work)

        try:
            app.dispatch.send_work(work)
        except NoWorkers:
            log.error("Failed to send work %s. No workers connected.", work.id)

            try:
                work.future.set_exception(NoWorkers("No workers connected"))
            except asyncio.exceptions.InvalidStateError:
                # ignore if the future was already set with exception,
                # that can happen in high contention environments
                pass

    async def handle_work_response(self, data):
        try:
            assert data is not None
            work_id = uuid.UUID(data["id"])
        except (AssertionError, KeyError, ValueError):
            raise InvalidData("Invalid work response data")

        recoverable: Optional[bool] = data.get("recoverable")
        failure_reason: str = data.get("failure_reason") or "<not given>"

        try:
            work = self.our_work_units.pop(work_id)
        except KeyError:
            log.warning("Got work response for unknown work ID %s", work_id)
            return

        if recoverable is None:
            http_response = HTTPWorkResponse(data["status_code"], data["response"])
            log.info("Got work %s response %r", work_id, http_response)
            work.future.set_result(http_response)
        elif recoverable:
            # we can still send this to another worker since its set to be
            # recoverable
            log.warning("Got work %s RECOVERABLE reason=%r", work_id, failure_reason)
            self._try_redispatch(work)

    async def handle_heartbeat(self, _data):
        await self.send_op(Op.HeartbeatAck, None)

    async def handle_heartbeat_ack(self, _data):
        # Getting a heartbeat ACK means to stop the task that's waiting for
        # the client's response. that's all.
        app.sched.stop(self._heartbeat_hope_task_id)

    async def handle_notify(self, data):
        try:
            notification_type = NotificationType(data["t"])
            notification_data = data["data"]
        except (KeyError, ValueError):
            raise InvalidData("Invalid notification message")

        notif_handler = self.NOTIFICATION_HANDLERS[notification_type]
        await notif_handler(notification_data)

    async def notif_banned_ip(self, notification_data):
        try:
            ip_addr = uuid.UUID(notification_data["ip"])
            topic = WorkTopic(notification_data["topic"])
        except (KeyError, ValueError):
            raise InvalidData("Invalid notification data")

        app.registry.mark_banned_ip(topic, ip_addr)

    async def handle(self, packet: Packet):
        try:
            op = Op(packet["op"])
        except (KeyError, ValueError):
            raise UnknownOpCode("No valid OP code found.")

        handler = self.HANDLERS[op]
        await handler(packet.get("d"))

    async def recv_loop(self):
        while True:
            packet = await self.recv()
            await self.handle(packet)

    def spawn_background_tasks(self):
        period_secs = round(self._hb_period / 1000, 2)
        log.info("Starting heartbeat task every %fs", period_secs)
        app.sched.spawn_periodic(
            self._call_heartbeat,
            [],
            period=period_secs,
            name=self._heartbeat_task_id,
            reverse=True,
        )

    def _destroy(self):
        # stop heartbeating tasks, as they're not needed anymore
        app.sched.stop(self._heartbeat_task_id)
        app.sched.stop(self._heartbeat_hope_task_id)

        # first, we need to remove the worker from the registry
        # so the pushed-back work units don't just come to this worker again.
        if self.worker_id is not None:
            app.registry.remove(self.worker_id)

        work_ids = list(self.our_work_units.keys())
        log.info("There are %d work units to recover.", len(work_ids))

        # push each work unit back to the network
        for work_id in work_ids:
            work = self.our_work_units[work_id]
            self._try_redispatch(work)
            self.our_work_units.pop(work_id)

    async def run(self):
        try:
            await self.send_hello()
            self.spawn_background_tasks()
            await self.recv_loop()
        except ConnectionClosedError as exc:
            log.warning(
                "Connection closed with code=%d, reason=%r", exc.code, exc.reason
            )

            # TODO destroy worker
        except WebsocketError as wse:
            log.warning("Closed connection. code=%d, reason=%r", wse.code, wse.message)
            await self.ws.close(code=wse.code, reason=wse.message)
        except Exception as err:
            log.exception("Generic error happened")
            await self.ws.close(code=4000, reason=repr(err))
        finally:
            self._destroy()
