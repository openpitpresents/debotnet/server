# debotnet: Distributed proxy
# Copyright 2020, lavatech and debotnet contributors
# SPDX-License-Identifier: AGPL-3.0-only

import urllib.parse
from .connection import WebsocketConnection


async def websocket_handler(app, ws, url):
    args = urllib.parse.parse_qs(urllib.parse.urlparse(url).query)

    try:
        protocol_version = args["v"][0]
    except (KeyError, IndexError):
        protocol_version = "1"

    if protocol_version not in ("1"):
        return await ws.close(code=4005, reason="Invalid protocol version")

    async with app.app_context():
        ws_wrapper = WebsocketConnection(ws)
        await ws_wrapper.run()
