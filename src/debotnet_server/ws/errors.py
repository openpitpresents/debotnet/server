# debotnet: Distributed proxy
# Copyright 2020, lavatech and debotnet contributors
# SPDX-License-Identifier: AGPL-3.0-only

class WebsocketError(Exception):
    code = 4000

    @property
    def message(self):
        return self.args[0]


class FailedAuth(WebsocketError):
    code = 4001


class Reconnect(WebsocketError):
    code = 4002


class UnknownOpCode(WebsocketError):
    code = 4003


class InvalidData(WebsocketError):
    code = 4005


class TooManyIPs(WebsocketError):
    code = 4006
