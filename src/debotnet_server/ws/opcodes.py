# debotnet: Distributed proxy
# Copyright 2020, lavatech and debotnet contributors
# SPDX-License-Identifier: AGPL-3.0-only

from enum import Enum


class Op(Enum):
    Hello = 0
    Identify = 1
    Ready = 2
    Heartbeat = 3
    HeartbeatAck = 4
    Work = 5
    WorkResponse = 6
    Notify = 7
