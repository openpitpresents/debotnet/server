# debotnet: Distributed proxy
# Copyright 2020, lavatech and debotnet contributors
# SPDX-License-Identifier: AGPL-3.0-only

import os
import logging
import json
import asyncio
from typing import Tuple

import websockets
import quart
from quart import Quart, jsonify
from violet import JobManager

from .ws.handler import websocket_handler
from .worker_registry import WorkerRegistry
from .dispatcher import WorkDispatcher
from .blueprints.api import bp as api_bp
from .api_errors import APIError

log = logging.getLogger(__name__)


def make_app():
    app = Quart(__name__)

    config_path = os.environ.get("DBT_CONFIG_PATH", "./config.json")
    with open(config_path) as f:
        app.cfg = json.load(f)

    is_debug = app.cfg.get("debug", False)
    app.debug = is_debug

    if is_debug:
        logging.basicConfig(level=logging.DEBUG)
        log.info("on debug")
        app.logger.level = logging.DEBUG
    else:
        logging.basicConfig(level=logging.INFO)

    # always keep websockets on INFO
    logging.getLogger("websockets").setLevel(logging.INFO)

    return app


def setup_blueprints(app):
    blueprints = [api_bp]

    for bp in blueprints:
        app.register_blueprint(bp)


app = make_app()
setup_blueprints(app)


def start_websocket(host, port, ws_handler) -> asyncio.Future:
    """Start a websocket. Returns the websocket future"""
    log.info(f"starting websocket at {host} {port}")

    async def _wrapper(ws, url):
        # We wrap the main websocket_handler
        # so we can pass quart's app object.
        await ws_handler(app, ws, url)

    return websockets.serve(_wrapper, host, port)


def create_app_singletons():
    app.registry = WorkerRegistry()
    app.dispatch = WorkDispatcher()


def _wrap_err_in_json(err: APIError) -> Tuple[quart.wrappers.Response, int]:
    res = {"error": True, "code": err.status_code, "message": err.message}
    res.update(err.payload)
    return jsonify(res), err.status_code


@app.errorhandler(APIError)
async def handle_api_error(err: APIError):
    """Handle any kind of application-level raised error."""
    log.warning(f"API error: {err!r}")
    return _wrap_err_in_json(err)


@app.errorhandler(Exception)
async def handle_any_error(err: Exception):
    """Handle any kind of application-level raised error."""

    if isinstance(err, quart.exceptions.NotFound):
        return "Not found", 404
    elif isinstance(err, quart.exceptions.HTTPException):
        log.exception("General error")
        return (
            jsonify({"error": True, "unexpected": True, "message": err.description}),
            err.status_code,
        )

    log.exception("General error")
    return jsonify({"error": True, "unexpected": True, "message": repr(err)}), 500


@app.before_serving
async def app_before_serving():
    app.loop = asyncio.get_event_loop()
    app.sched = JobManager(loop=None, context_function=app.app_context)
    create_app_singletons()

    # start gateway websocket
    # voice websocket is handled by the voice server
    ws_addr = app.cfg["websocket_bind"]
    await start_websocket(ws_addr[0], ws_addr[1], websocket_handler)
