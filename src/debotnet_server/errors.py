# debotnet: Distributed proxy
# Copyright 2020, lavatech and debotnet contributors
# SPDX-License-Identifier: AGPL-3.0-only


class NoWorkers(Exception):
    """There are no connected workers."""

    pass


class TooManyRetries(Exception):
    """Work units have a total amount of times they can
    be thrown around to workers in the system."""

    pass


class WorkerError(Exception):
    """An error happened on the request."""

    pass
