# debotnet: Distributed proxy
# Copyright 2020, lavatech and debotnet contributors
# SPDX-License-Identifier: AGPL-3.0-only

import asyncio
import uuid
import logging
import random

from enum import Enum
from typing import List, Dict, Any
from dataclasses import dataclass

from quart import current_app as app
from .errors import TooManyRetries

log = logging.getLogger(__name__)


class WorkTopic(Enum):
    # Minecraft Username->UUID resoltuion
    Username = "username"

    # Check if a session exists, for Minecraft server authentication flow
    HasJoined = "has_joined"

    # Get ipinfo.io info
    DebugIpInfo = "debug_ipinfo"


@dataclass
class Work:
    id: uuid.UUID
    typ: WorkTopic
    data: Any
    future: asyncio.Future
    retries: int = 0

    def __repr__(self):
        return f"Work<id={self.id!s} type={self.typ!r} data={self.data!r}>"


class WorkDispatcher:
    """Dispatch a work unit to a connected Worker."""

    def __init__(self):
        pass

    def _create_work(self, work_type: WorkTopic, work_data: Any) -> asyncio.Future:
        future: asyncio.Future = asyncio.Future(loop=app.loop)
        work = Work(uuid.uuid4(), work_type, work_data, future)
        self.send_work(work)
        return future

    def send_work_username(self, username_list: List[str]) -> asyncio.Future:
        return self._create_work(WorkTopic.Username, username_list)

    def send_work_has_joined(self, arguments: Dict[str, str]) -> asyncio.Future:
        return self._create_work(WorkTopic.HasJoined, arguments)

    def send_work_debug_ipinfo(self, _) -> asyncio.Future:
        return self._create_work(WorkTopic.DebugIpInfo, [])

    def send_work(self, work: Work) -> None:
        if work.retries > app.cfg["max_work_retries"]:
            raise TooManyRetries()

        worker_id, worker = app.registry.get_wanted_worker(work.typ)
        log.info("Pushing work %s to worker %r", work.typ, worker_id)

        work.retries += 1
        worker.conn.push_work(work)

        ratelimited_addr = random.choice(worker.ip_addresses)
        app.registry.mark_ratelimited_ip(work.typ, ratelimited_addr)
