# debotnet: Distributed proxy
# Copyright 2020, lavatech and debotnet contributors
# SPDX-License-Identifier: AGPL-3.0-only

import asyncio
import logging
from typing import Any, Tuple

from quart import Blueprint, jsonify, request, current_app as app

from ..api_errors import (
    BadRequest,
    NoWorkers as APINoWorkers,
    TooManyRetries as APITooManyRetries,
    TimeoutError as APITimeoutError,
)
from ..errors import NoWorkers, TooManyRetries

log = logging.getLogger(__name__)
bp = Blueprint("usernames", __name__)


async def _send_and_wait(send_method, j, *, timeout=30) -> Tuple[Any, int]:
    # We translate the internal errors into an API Error because
    # those are less verbose on the logs.
    try:
        future = send_method(j)
        resp = await asyncio.wait_for(future, timeout=timeout)
        return jsonify(resp.data), resp.status_code
    except asyncio.TimeoutError:
        raise APITimeoutError("Timed out waiting for a response")
    except NoWorkers as exc:
        raise APINoWorkers(*exc.args)
    except TooManyRetries as exc:
        raise APITooManyRetries(*exc.args)


@bp.route("/profiles/minecraft", methods=["POST"])
async def resolve_usernames():
    j = await request.get_json()
    if not isinstance(j, list):
        raise BadRequest("Given JSON is not a list.")

    all_str = all(isinstance(username, str) for username in j)
    if not all_str:
        raise BadRequest("One or more of the items are not strings.")

    return await _send_and_wait(app.dispatch.send_work_username, j)


@bp.route("/session/minecraft/hasJoined")
async def has_joined():

    # username and serverId are required, ip can be missing
    args = {"username": request.args["username"], "serverId": request.args["serverId"]}
    if "ip" in request.args:
        args["ip"] = request.args["ip"]

    return await _send_and_wait(app.dispatch.send_work_has_joined, args)


@bp.route("/debug/ipinfo")
async def debug_ipinfo():
    # TODO: might want to move this to a separate file eventually
    if not app.cfg["debug"]:
        return "Server not on debug mode", 403
    return await _send_and_wait(app.dispatch.send_work_debug_ipinfo, [])


@bp.route("/api/status")
async def server_status():
    if not app.cfg["expose_status"]:
        return "Server does not expose status", 403

    return jsonify(app.registry.get_status())
