from setuptools import setup, find_packages

setup(
    name="debotnet_server",
    version="0.1.0",
    description="Distributed Proxy (server component)",
    license="AGPLv3",
    url="https://gitlab.com/openpitpresents/debotnet/debotnet-server",
    author="Ave Ozkal, Luna Mendes, Eden Segal-Grossman",
    python_requires=">=3.8",
    packages=find_packages("src"),
    package_dir={"": "src"},
    py_modules=["debotnet_server"],
    install_requires=[
        "hypercorn==0.9.5",
        "Quart==0.11.5",
        "cerberus==1.3.2",
        "aiohttp==3.6.2",
        "websockets==8.1",
        "aioinflux==0.9.0",
        "expiringdict==1.2.0",
        "violet @ git+https://gitlab.com/elixire/violet.git@0d0ed4cd3976a6f92d9ac6cc66dbe72cac544544#egg=violet",
        "hail @ git+https://gitlab.com/elixire/hail.git@d72895019ef68eb96bb775f939182dd9344de36#egg=hail",
    ],
)
