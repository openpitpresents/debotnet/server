# De Botnet (C&C / Server component)

Distributed proxy system.

This is the Server component, which manages distribution of work units to
a number of workers connected to it. To have the entire project running, you
need to both setup a Server and a [Worker](https://gitlab.com/openpitpresents/debotnet/worker).

```bash
# edit config.json as wanted
cp config.template.json config.json

# Older pip has a bug where it pulls violet/hail from pypi instead of git (sigh)
python3.8 -m pip install -U pip

# install depedencies
python3.8 -m pip install --editable .

# run the server:
hypercorn --access-log - --bind :::8080 debotnet_server.__main__:app
```

## Docker instructions

```
# To build:
docker build -t debotnet-server .

# To run:
docker run --env DBT_CONFIG_PATH=config.template.json -it --rm --name debotnet-server debotnet-server
# You'll likely want to mount a different folder for config and point it to that in prod tho.
``` 
